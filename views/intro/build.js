var ManageDOM = require('../../scripts/manageDOM')
  , Static = require('../game/static')

ManageDOM.createPage('intro', require('./template'))
ManageDOM.showPage('intro')


var unlockedStage = localStorage.getItem('unlockedStage')
if(!unlockedStage) {
	unlockedStage = 1
	localStorage.setItem('unlockedStage', unlockedStage)
} else {
	unlockedStage = JSON.parse(unlockedStage)
}

loadStage(unlockedStage)

function loadStage(unlockedStage) {
	console.log(localStorage.getItem('hiscores'))
	var hiscores = JSON.parse(localStorage.getItem('hiscores') || '[]')

	var stageIndex = 1

	for(stageIndex=1; stageIndex<=unlockedStage; stageIndex++) {
		createStage(stageIndex)
	}

	function createStage(stageIndex) {
		ManageDOM.append('levels', '<div class="stageTitle">'+Static.stages[stageIndex].name+'</div>')

		var levelsNum = Object.keys(Static.stages[stageIndex].levels).length

		for(var levelI=1; levelI<=levelsNum; levelI++) {
			createLevel(stageIndex, levelI)
		}
	}

	function createLevel(stageIndex, levelIndex) {
		var levelVariable = 'level'+stageIndex+'.'+levelIndex
		  , hiscore = ''

		if(hiscores[stageIndex-1] && hiscores[stageIndex-1][levelIndex]) {
			hiscore = hiscores[stageIndex-1][levelIndex]
		}


		ManageDOM.append('levels', '<div class="level" data-variable="'+levelVariable+'">Level '+levelIndex+'<div data-variable="'+levelVariable+'hiscore'+'">'+hiscore+'</div></div>')

		ManageDOM.attachListeners(levelVariable, 'click', function() {
			ManageDOM.showPage('game', { stageIndex:stageIndex, levelIndex:levelIndex })
		})
	}
}