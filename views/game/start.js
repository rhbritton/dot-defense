var Static = require('./static')
  , ManageDOM = require('../../scripts/manageDOM')
  , Save = require('../../scripts/save')
  , Spawning = require('./scripts/spawning')
  , Init = require('./scripts/init')

module.exports.start = function(data) {
	var staticLevel = Static.stages[data.stageIndex || 1].levels[data.levelIndex || 1]
	  , stageStage =  Static.stages[data.stageIndex || 1]
	
	var locals = {
		inGame: false,
		endedGame: false,
		currentTime: null,
		startTime: null,
		stage: null,
		castle: null,
		timeText: null,
		killsText: null,
		killsLeftText: null,
		levelText: null,
		nextWaveText: null,
		waveCompleteText: null,
		nextWaveButton: null,
		levelStage: data.stageIndex || 1,
		stageStage: stageStage,
		levelLevel: data.levelIndex || 1,
		level: staticLevel,
		kills: null,
		spawnsLeft: staticLevel.enemies,
		killsLeft: null,
		enemies: null,
		dyingEnemies: null,
		enemySpawnTime: null,
		castleStats: null,
		currentLevel: null,
		playNextWave: null,
		handleTick: null,
		selectedEnemy: null,
		enemyHealthBar: null,
		enemyHealthBarFiller: null
	}

	Init.initialize(locals)

	locals.handleTick = require('./scripts/handleTick').handleTick.bind(this, locals)
	createjs.Ticker.setFPS(30)
	createjs.Ticker.addEventListener("tick", locals.handleTick)
	
}