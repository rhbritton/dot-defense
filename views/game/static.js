var Static = module.exports

Static.clickDamage = function() {
	return 1
}

Static.castle = {
	height: 50
}

Static.enemies = {
	'peasant': {
		health: 3, // clicks to kill
		score: 3,
		attack: 1, // damage per second
		speed: 40, // pixels per second
		height: 50,
		width: 50,
		spriteSheet: {
			images: ['./images/enemies/peasant/full.png'],
		    frames: { width:50, height:50, regX: 0, regY: 0, count: 36 },
		    animations: {
		    	walk:{
		    		frames:[0,1,2,3,4,5,6,7,8,9,10,11],
		    		speed: 0.15
		    	},
		    	attack: {
		    		frames:[12,13,14,15,16,17,18,19,20,21,22,23],
		    		speed: 0.15
		    	},
		    	die: {
		    		frames:[24,25,26,27,28,29,30,31,32,33,34,35],
		    		speed: 0.15
		    	}
		    }
		}
	},
	'farmer': {
		health: 5, // clicks to kill
		score: 5,
		attack: 2, // damage per second
		speed: 50, // pixels per second
		height: 50,
		width: 50,
		spriteSheet: {
			images: ['./images/enemies/peasant/full.png'],
		    frames: { width:50, height:50, regX: 0, regY: 0, count: 36 },
		    animations: {
		    	walk:{
		    		frames:[0,1,2,3,4,5,6,7,8,9,10,11],
		    		speed: 0.15
		    	},
		    	attack: {
		    		frames:[12,13,14,15,16,17,18,19,20,21,22,23],
		    		speed: 0.15
		    	},
		    	die: {
		    		frames:[24,25,26,27,28,29,30,31,32,33,34,35],
		    		speed: 0.15
		    	}
		    }
		}
	},
	'orc': {
		health: 7, // clicks to kill
		score: 7,
		attack: 3, // damage per second
		speed: 60, // pixels per second
		height: 50,
		width: 50,
		spriteSheet: {
			images: ['./images/enemies/peasant/full.png'],
		    frames: { width:50, height:50, regX: 0, regY: 0, count: 36 },
		    animations: {
		    	walk:{
		    		frames:[0,1,2,3,4,5,6,7,8,9,10,11],
		    		speed: 0.15
		    	},
		    	attack: {
		    		frames:[12,13,14,15,16,17,18,19,20,21,22,23],
		    		speed: 0.15
		    	},
		    	die: {
		    		frames:[24,25,26,27,28,29,30,31,32,33,34,35],
		    		speed: 0.15
		    	}
		    }
		}
	},
	'giant': {
		health: 20, // clicks to kill
		attack: 4, // damage per second
		score: 10,
		speed: 20, // pixels per second
		height: 50,
		width: 50,
		spriteSheet: {
			images: ['./images/enemies/peasant/full.png'],
		    frames: { width:50, height:50, regX: 0, regY: 0, count: 36 },
		    animations: {
		    	walk:{
		    		frames:[0,1,2,3,4,5,6,7,8,9,10,11],
		    		speed: 0.15
		    	},
		    	attack: {
		    		frames:[12,13,14,15,16,17,18,19,20,21,22,23],
		    		speed: 0.15
		    	},
		    	die: {
		    		frames:[24,25,26,27,28,29,30,31,32,33,34,35],
		    		speed: 0.15
		    	}
		    }
		}
	}
}

Static.levelRates = {
	enemyRate: 1.3, // dividing rate
	enemies: 5 // add enemies
}
Static.stages = {
	1: {
		name:'Grasslands',
		levels: {
			1: {
				enemyRate: 1500,
				amount: 1,
				enemies: 1,
				enemyTypes: [{
					type: 'peasant',
					chance: {
						low: 0,
						high: 1
					}
				}]
			},
			2: {
				enemyRate: 1000,
				amount: 1,
				enemies: 15,
				enemyTypes: [{
					type: 'peasant',
					chance: {
						low: 0,
						high: 1
					}
				}]
			},
			3: {
				enemyRate: 1000,
				amount: 1,
				enemies: 20,
				enemyTypes: [{
					type: 'peasant',
					chance: {
						low: 0,
						high: 1
					}
				}]
			}
		}
	}
}