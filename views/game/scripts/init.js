var Init = module.exports
  , Static = require('../static')

Init.initialize = function(locals) {
	locals.currentTime = Date.now()
	locals.startTime = locals.currentTime
	locals.stage = new createjs.Stage('canvas')
	locals.castle = new createjs.Bitmap("../../images/wall.png")

	locals.scoreText = new createjs.Text("Score: 0", "20px Arial", "black")

	locals.levelText = new createjs.Text(locals.stageStage.name + ' ' + locals.levelLevel, "20px Arial", "black")
	locals.enemyHealthBar = new createjs.Shape()
	locals.enemyHealthBarFiller = new createjs.Shape()
	locals.grass = new createjs.Bitmap("../../images/grass.png")

	locals.wallHealthBar = new createjs.Shape()
	locals.wallHealthBarFiller = new createjs.Shape()

	locals.kills = 0
	locals.score = 0
	locals.killsLeft = 0
	locals.enemies = {}
	locals.enemiesOrder = []
	locals.dyingEnemies = {}
	locals.dyingEnemiesOrder = []
	locals.enemySpawnTime = 1000 + Date.now()
	locals.castleStats = {
		health: 100
	}

	locals.inGame = true


	var container = document.querySelector('.container')

	locals.stage.canvas.width = parseInt(container.clientWidth)
   	locals.stage.canvas.height = parseInt(container.clientHeight)

   	locals.castleStats.x = 0
   	locals.castleStats.y = locals.stage.canvas.height - Static.castle.height
   	locals.castleStats.width = locals.stage.canvas.width
   	locals.castleStats.height = Static.castle.height*2

 	locals.levelText.x = 10
 	locals.levelText.y = 10

 	locals.scoreText.x = 10
 	locals.scoreText.y = 35

	locals.castle.x = 0
	locals.castle.y = locals.stage.canvas.height - 65

	locals.wallHealthBarFiller.graphics.clear().beginFill('#222').drawRect(9, 99, 12, locals.stage.canvas.height - 198)

}

Init.buildEnvironment = function(locals) {
	locals.stage.addChild(locals.grass)

	var healthPercentage = locals.castleStats.health/100
	  , healthHeight = locals.stage.canvas.height - 200

	locals.wallHealthBar.graphics.clear().beginFill('#0c0').drawRect(10, 100+(healthHeight - (healthHeight*healthPercentage)), 10, healthHeight*healthPercentage)
	
	locals.stage.addChild(locals.wallHealthBarFiller)
	locals.stage.addChild(locals.wallHealthBar)

	locals.stage.addChild(locals.timeText)
	locals.stage.addChild(locals.scoreText)
	locals.stage.addChild(locals.levelText)

	if(!locals.killsLeft) {
		locals.stage.addChild(locals.nextWaveButton)
		locals.stage.addChild(locals.nextWaveText)
		locals.stage.addChild(locals.waveCompleteText)
	} else {
		locals.stage.addChild(locals.killsLeftText)
	}
}

Init.buildWall = function(locals) {
	locals.stage.addChild(locals.castle)
}