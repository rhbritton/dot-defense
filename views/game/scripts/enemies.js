var Enemies = module.exports
  , Static = require('../static')
  , ObjectManipulation = require('./objectManipulation')
  , Enemies = require('./enemies')

Enemies.init = function() {

}

Enemies.shouldEnemyAttack = function(enemy, secondsDiff, locals) {
	var collision = ObjectManipulation.checkCollision(enemy, locals)

	if(locals.castleStats.health > 0 && (collision || enemy.y == locals.stage.canvas.height-10)) {
		locals.castleStats.health -= secondsDiff*enemy.attack
		if(locals.castleStats.health < 0) locals.castleStats.health = 0
	}
}

Enemies.moveEnemies = function(oldTime, locals) {
	var timeDiff = locals.currentTime - oldTime
	  , percent = timeDiff/1000
	
	for(key in locals.enemies) {
		var enemy = locals.enemies[key]

		var newY = locals.enemies[key].y + (locals.enemies[key].speed * percent)

		if(enemy.y > locals.stage.canvas.height-10)
			enemy.y = locals.stage.canvas.height - 10
		

		if(ObjectManipulation.checkCollision(enemy, locals) || enemy.y == locals.stage.canvas.height-10) {
			if(!enemy.attacking) {
				enemy.animation.gotoAndPlay("attack")
			}
			enemy.attacking = true
			
			// Attack
		} else {
			locals.enemies[key].y = newY
		}
	}
}

Enemies.drawEnemies = function(secondsDiff, locals) {

	Enemies.updateSortedEnemies(locals)
	Enemies.updateSortedDyingEnemies(locals)

	for(var i=0; i<locals.dyingEnemiesOrder.length; i++) {
		var key = locals.dyingEnemiesOrder[i]
		  , enemy = locals.dyingEnemies[key]

		if(enemy.timeOfDeath <= locals.currentTime) {
			delete locals.dyingEnemies[key]
		} else {
			var percentage = ((enemy.timeOfDeath - locals.currentTime)/1000)

			enemy.animation.x = enemy.x
		    enemy.animation.y = enemy.y
		    enemy.animation.alpha = percentage
			locals.stage.addChild(enemy.animation)


			var addedScoreText = new createjs.Text("+"+enemy.score, "bold 15px Arial", "rgba(200, 0, 0, "+percentage+")")
			addedScoreText.x = enemy.x + enemy.width/2 - 10
			addedScoreText.y = enemy.y - 10 + percentage*50 - 50

			locals.stage.addChild(addedScoreText)
		}
	}


	for(var i=0; i<locals.enemiesOrder.length; i++) {
		var key = locals.enemiesOrder[i]
		  , enemy = locals.enemies[key]

		if(key == locals.selectedEnemy) {
			locals.enemyHealthBarFiller.graphics.clear().beginFill('#222').drawRect(enemy.x-1, enemy.y-11, enemy.height+2, 7)
			locals.enemyHealthBar.graphics.clear().beginFill('#0c0').drawRect(enemy.x, enemy.y-10, enemy.height*(enemy.health/enemy.fullHealth), 5)
			locals.stage.addChild(locals.enemyHealthBarFiller)
    		locals.stage.addChild(locals.enemyHealthBar)
		}

		enemy.animation.x = enemy.x
	    enemy.animation.y = enemy.y
    	locals.stage.addChild(enemy.animation)

		Enemies.shouldEnemyAttack(enemy, secondsDiff, locals)
	}

}

Enemies.updateSortedEnemies = function(locals) {
	var arr = Object.keys(locals.enemies)

	arr.sort(function(a, b) {
		return locals.enemies[a].y > locals.enemies[b].y
	})

	locals.enemiesOrder = arr
}

Enemies.updateSortedDyingEnemies = function(locals) {
	var arr = Object.keys(locals.dyingEnemies)

	arr.sort(function(a, b) {
		return locals.dyingEnemies[a].timeOfDeath > locals.dyingEnemies[b].timeOfDeath
	})

	locals.dyingEnemiesOrder = arr
}