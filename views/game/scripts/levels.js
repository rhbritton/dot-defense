var Levels = module.exports
  , Static = require('../static')

Levels.checkLevel = function(locals) {
	if(!locals.killsLeft && locals.playNextWave) {
		locals.playNextWave = false
		if(!locals.currentLevel) {
			locals.currentLevel = 1
			locals.level = Static.levels[locals.currentLevel]
		} else {
			locals.currentLevel++

			var enemyRate = locals.level.enemyRate/Static.levelRates.enemyRate
			  , enemies = locals.level.enemies + Static.levelRates.enemies
			  , amount = locals.level.amount + 1

			if(!Static.levels[locals.currentLevel]) {
				locals.level = {
					enemyRate: enemyRate,
					enemies: enemies,
					amount: amount
				}
			} else {
				locals.level = Static.levels[locals.currentLevel]

				if(!locals.level.enemyRate) locals.level.enemyRate = enemyRate
				if(!locals.level.enemies) locals.level.enemies = enemies
				if(!locals.level.amount) locals.level.amount = amount
			}
		}

		locals.killsLeft = locals.spawnsLeft = locals.level.enemies
	}
}