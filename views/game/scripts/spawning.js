var Spawning = module.exports
  , Static = require('../static')
  , Enemies = require('./enemies')

Spawning.spawnCheck = function(locals) {
	if(locals.currentTime >= locals.enemySpawnTime && locals.spawnsLeft) {
		locals.spawnsLeft--
		Spawning.spawnEnemy(locals, onEnemyClick)
	}

	function onEnemyClick(key) {
		locals.selectedEnemy = key

		var enemy = locals.enemies[key]

		enemy.health--

		if(enemy && enemy.health <= 0) {
			locals.enemies[key].animation.removeAllEventListeners('click')
			locals.dyingEnemies[key] = locals.enemies[key]
			locals.dyingEnemies[key].animation.gotoAndPlay("die")
			locals.dyingEnemies[key].timeOfDeath = locals.currentTime + 1000

			locals.score += locals.enemies[key].score

			delete locals.enemies[key]

			locals.killsLeft--
			return locals.kills++
		}

		for(k in Static.enemies[enemy.level]) {
			if(k != 'x' || k != 'y' || k != 'shape')
				locals.enemies[key][k] = Static.enemies[enemy.level][k]
		}
	}
}

Spawning.spawnEnemy = function(locals, fn) {
	// reset spawntime
	var seconds = Math.random() * locals.level.enemyRate*2
	locals.enemySpawnTime = seconds + locals.currentTime

	// spawn the enemy
	spawn()

	// return new spawn time
	return


	function spawn() {
		var key = Math.floor(Math.random()*100000000000000000)
		  , enemyType = calcEnemyType()
		  , enemy = locals.enemies[key] = {}

		for(k in Static.enemies[enemyType]) {
			if(k != 'x' || k != 'y' || k != 'shape')
				locals.enemies[key][k] = Static.enemies[enemyType][k]
		}

		enemy.fullHealth = locals.enemies[key].health
		enemy.spawnTime = locals.currentTime

		enemy.x = (Math.random() * (0.9 - 0.1) + 0.1)*locals.stage.canvas.width
		enemy.y = 0

		enemy._spriteSheet = new createjs.SpriteSheet(enemy.spriteSheet)
		enemy.animation = new createjs.Sprite(enemy._spriteSheet, "walk")
    	enemy.animation.gotoAndPlay("walk")

    	enemy.animation.on('click', function() {
    		fn(key)
    	})
	}

	function calcEnemyType() {
		var types = locals.level.enemyTypes
		  , rand = Math.random()

		if(!locals.level.enemyTypes) {
			var enemyIndices = Object.keys(Static.enemies)
			return enemyIndices[Math.floor(enemyIndices.length*rand)]
		}

		for(var i=0; i<types.length; i++) {
			if(rand >= types[i].chance.low && rand <= types[i].chance.high) {
				return types[i].type
			}
		}
	}

}

	