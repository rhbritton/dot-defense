var End = module.exports
  , Static = require('../static')
  , ManageDOM = require('../../../scripts/manageDOM')
  , HandleTick = require('./handleTick')
  , Save = require('../../../scripts/save')

End.stopGame = function(locals) {
	createjs.Ticker.removeEventListener("tick", locals.handleTick)
}

End.endGame = function(locals) {
	setTimeout(function() {
		createjs.Ticker.removeEventListener("tick", locals.handleTick)

		var hiscores = JSON.parse(localStorage.getItem('hiscores') || '[]')
		  , isHiscore = !hiscores[locals.levelStage-1] || parseInt(hiscores[locals.levelStage-1][locals.levelLevel] || 0) < locals.score

		if(isHiscore) {
			if(!hiscores[locals.levelStage-1]) hiscores[locals.levelStage-1] = {}

			hiscores[locals.levelStage-1][locals.levelLevel] = locals.score
			Save.variable('hiscores', JSON.stringify(hiscores))
			ManageDOM.setVariable('level'+locals.levelStage+'.'+locals.levelLevel+'hiscore', locals.score)
		}

		var gameResult = ''

		if(isHiscore) gameResult = 'New Hiscore! '
		
		gameResult += 'You got a score of ' + locals.score + '!'
		ManageDOM.setVariable('gameResult', gameResult)

		ManageDOM.showPage('results')
	}, 1000)
}