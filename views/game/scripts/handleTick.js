var HandleTick = module.exports
  , Static = require('../static')
  , Levels = require('./levels')
  , End = require('./end')
  , Init = require('./init')
  , ObjectManipulation = require('./objectManipulation')
  , Spawning = require('./spawning')
  , Enemies = require('./enemies')

HandleTick.handleTick = function(locals) {
	if(!locals.inGame) return End.stopGame(locals)
	if(!locals.castleStats.health && !locals.endedGame) {
		locals.endedGame = true
		End.endGame(locals)
	}
	if(!locals.spawnsLeft && !Object.keys(locals.enemies).length && !locals.endedGame) {
		locals.endedGame = true
		End.endGame(locals)
	}

	var oldTime = locals.currentTime
	locals.currentTime = Date.now()

	var secondsDiff = (locals.currentTime - oldTime)/1000

	var totalTimeDiff = Math.floor((locals.currentTime - locals.startTime)/1000)
	
	ObjectManipulation.changeScoreText(locals)

	Enemies.moveEnemies(oldTime, locals)

	Spawning.spawnCheck(locals)
	
	// Draw
	locals.stage.removeAllChildren()
	
	Init.buildEnvironment(locals)
	Enemies.drawEnemies(secondsDiff, locals)
	Init.buildWall(locals)

	locals.stage.update()
}