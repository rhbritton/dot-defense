var ObjectManipulation = module.exports
  , Static = require('../static')
  , sortObject = require('sort-object')

ObjectManipulation.changeTimeText = function(totalTimeDiff, locals) {
	var seconds = totalTimeDiff % 60
	  , minutes = Math.floor(totalTimeDiff/60)
	  , text = ''

	if(minutes < 10)
		text += '0'

	text += minutes + ':'

	if(seconds < 10)
		text += '0'
	
	text += seconds

	locals.timeText.text = text
}

ObjectManipulation.changeScoreText = function(locals) {
	var text = 'Score: ' + locals.score
	locals.scoreText.text = text
}

ObjectManipulation.checkCollision = function(enemy, locals) {
	return enemy.y + enemy.height >= locals.castleStats.y
}

ObjectManipulation.changeLevelText = function(locals) {
	var text = 'Level ' + locals.currentLevel
	locals.levelText.text = text
}

ObjectManipulation.changeNextWaveText = function(locals) {
	var text = 'Next Wave: ' + locals.killsLeft
	locals.killsLeftText.text = text
}

ObjectManipulation.changeWaveCompleteText = function(locals) {
	var text = 'You Completed Wave ' + locals.level.amount
	locals.waveCompleteText.text = text
}