var save = module.exports
  , ManageDOM = require('./manageDOM')

save.variable = function(name, value) {
	localStorage.setItem(name, value)
	ManageDOM.setVariable(name, localStorage.getItem(name))
}