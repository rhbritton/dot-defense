var manageDOM = module.exports

manageDOM.createPage = function(name, template) {
	var div = document.createElement('div')
	div.id = name
	div.className = 'page'
	div.style.display = 'none'
	document.getElementsByTagName('body')[0].appendChild(div)
	if(template)
		div.innerHTML = template
}

manageDOM.showPage = function(name, data) {
	var pages = document.querySelectorAll('.page')
	  , page = document.getElementById(name)
	  , start = require('../views/'+name+'/start')

	;[].forEach.call(pages, function(page) {
		page.style.display = 'none'
	})

	page.style.display = 'block'

	if(start)
		return start.start(data)
}

manageDOM.overlayPage = function(backgroundName, overlayName, opacity) {
	var pages = document.querySelectorAll('.page')
	  , backgroundPage = document.getElementById(backgroundName)
	  , overlayPage = document.getElementById(overlayName)
	  , start = require('../views/'+overlayName+'/start')

	;[].forEach.call(pages, function(page) {
		page.style.display = 'none'
		page.style.zIndex = 0
	})

	backgroundPage.style.display = 'block'

	overlayPage.style.display = 'block'
	overlayPage.style.zIndex = 1
	overlayPage.style.background = 'rgba(0, 0 f, 0.75)'

	if(start)
		return start.start()
}

manageDOM.setVariable = function(variable, value) {
	var variables = document.querySelectorAll('[data-variable="'+variable+'"]')

	;[].forEach.call(variables, function(variable) {
		variable.innerText = value
	})
}

manageDOM.attachListeners = function(variable, action, fn) {
	var variables = document.querySelectorAll('[data-variable="'+variable+'"]')

	;[].forEach.call(variables, function(variable) {
		variable.addEventListener(action, fn)
	})
}

manageDOM.append = function(variable, child) {
	var variables = document.querySelectorAll('[data-variable="'+variable+'"]')
	  , remaining = variables.length

	;[].forEach.call(variables, function(variable, i) {
		var div = document.createElement('div')
		div.innerHTML = child
		variables[i].appendChild(div.childNodes[0])
	})
}